DROP TABLE FEDUINO.device_generic_data;
DROP TABLE FEDUINO.device;
DROP TABLE FEDUINO.device_type;
-- device type
CREATE TABLE FEDUINO.device_type(id NUMBER(2) PRIMARY KEY,dscp VARCHAR2(30));
-- generic device
CREATE TABLE FEDUINO.device(id VARCHAR2(4) PRIMARY KEY,
                            dscp VARCHAR2(30),
                            device_type_id NUMBER(2),
                            last_online DATE,
                            started DATE,
                            free_mem NUMBER(10));
ALTER TABLE FEDUINO.device ADD CONSTRAINT device_fk1
FOREIGN KEY (device_type_id) REFERENCES  FEDUINO.device_type(id);
CREATE TABLE FEDUINO.device_generic_data (id NUMBER ,
                                  device_id VARCHAR2(4),
                                  insert_tmttmp DATE,
                                  num1 NUMBER,
                                  num2 NUMBER,
                                  num3 NUMBER,
                                  str1 NUMBER,
                                  str2 NUMBER,
                                  str3 NUMBER);
ALTER TABLE FEDUINO.device_generic_data ADD PRIMARY KEY (id,device_id);
ALTER TABLE FEDUINO.device_generic_data ADD CONSTRAINT device_generic_data_fk1
FOREIGN KEY (device_id) REFERENCES  FEDUINO.device(id);





