package com.adipro.schedule;

import com.adipro.email.EmailSender;
import com.adipro.server.Server;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.time.LocalDateTime;

@RequestScoped
public class ScheduleService {
    @EJB
    Server server;

    @Inject
    EmailSender sender;

    public void onEvent(@Observes TimeEvent event) {
        server.setLastHealthCheck(LocalDateTime.now());
        //sender.sendEmail();
    }
}
