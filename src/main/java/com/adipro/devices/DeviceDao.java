package com.adipro.devices;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;



public class DeviceDao {
    public static final String SELECTALL = "select d from Device d";

    private final static Logger LOGGER = Logger.getLogger(DeviceDao.class.getName());
    EntityManager em;

    public DeviceDao(EntityManager em) {
        this.em = em;
    }

    public List<Device> getAllDevices() {
        Query q = em.createQuery(DeviceDao.SELECTALL);
        return q.getResultList();
    }

    public void updateDevice(Device device) {
        em.persist(device);
        em.merge(device);
    }

    public Device findDevice(String id) {
        return em.find( Device.class, id);
    }

    public DeviceType findDeviceType(Integer deviceTypeId) {
        return em.find( DeviceType.class, deviceTypeId);
    }

    public Device addDevice(String id,String dscp,DeviceType deviceType) {
        DHTSensor device = new DHTSensor();
        device.setId(id);
        device.setDeviceType(deviceType);
        device.setLocation(dscp);
        em.persist(device);
        return device;

    }
}
