package com.adipro.devices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NetData {
    private String id;
    private String longVal;
    private List<String> shortVals = new ArrayList<>();
    private static final int SHORTVALLENGTH = 4;
    private static final int LONGVALLENGTH = 8;
    private static final int IDLENGTH = 4;
    private static final int TOTALMAXLENGTH = 21;
    private NetData() {}
    public static NetData from(String id) {
        NetData instance = new NetData();
        instance.id = id;
        return instance;
    }

    public static NetData fromRawData(String rawData) {
        NetData instance = new NetData();
        instance.id = rawData.substring(0,4);
        instance.longVal = rawData.substring(4,12);
        instance.shortVals.add(rawData.substring(12,16));
        instance.shortVals.add(rawData.substring(16));
        return instance;
    }

    public NetData setLongVal(String val) {
        this.longVal = val;
        return this;
    }

    public NetData setShortVal(String... val) {
        this.shortVals = Arrays.asList(val);
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(id);
        sb.append(padLeft(longVal,LONGVALLENGTH));
        shortVals.stream().forEach(val -> sb.append(padLeft(val,SHORTVALLENGTH)));
        return padRight(sb.toString(),TOTALMAXLENGTH);
    }

    public String toStringId() {
        return id;
    }

    public String getId() {
        return id;
    }

    public String getLongVal() {
        return longVal;
    }

    private String getShortVal(int id) {
        if (this.shortVals.size() > id) {
            return this.shortVals.get(id);
        }
        return null;
    }

    private int toInt(String str) {
        if (str == null ) {
            return 0;
        }
        else {
            return Integer.parseInt(str.trim());
        }
    }

    public String getShortVal1() {
        return getShortVal(0);
    }
    public int getShortIntVal1() {
        return toInt(getShortVal1());
    }

    public int getShortIntVal2() {
        return toInt(getShortVal2());
    }

    public String getShortVal2() {
        return getShortVal(1);
    }

    private static String padLeft(String s, int n) {
        return String.format("%" + n + "s", s);
    }

    private static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }
}
