package com.adipro.devices;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="device_type_id",
        discriminatorType = DiscriminatorType.INTEGER)
public abstract class Device {

    @Id
    @Column(name = "id")
    private String id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "device_type_id", referencedColumnName = "id")
    private DeviceType deviceType;

    @Column(name = "last_online")
    private LocalDateTime lastOnline;

    @Column(name = "started")
    private LocalDateTime started;

    @Column(name = "free_mem")
    private int freeMem;

    @OneToMany(mappedBy="device",cascade = CascadeType.ALL)
    @OrderBy("id DESC")
    private List<DeviceData> deviceDataList;

    public abstract NetData toNetData();
    protected abstract void updateFromNetData(NetData netData);

    public void update(NetData netData) {
        this.setLastOnline(LocalDateTime.now());
        updateFromNetData(netData);
    }
    public String getId() {
        return id;
    }

    public  DeviceType getType() {
        return deviceType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public LocalDateTime getLastOnline() {
        return lastOnline;
    }

    public List<DeviceData> getDeviceDataList() {
        return deviceDataList;
    }

    public LocalDateTime getStarted() {
        return started;
    }

    public void setStarted(LocalDateTime started) {
        this.started = started;
    }


    public int getFreeMem() {
        return freeMem;
    }

    public void setFreeMem(int freeMem) {
        this.freeMem = freeMem;
    }

    public void setLastOnline(LocalDateTime lastOnline) {
        this.lastOnline = lastOnline;
    }
}
