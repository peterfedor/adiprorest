package com.adipro.devices;

import com.adipro.common.Util;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.logging.Logger;


@Entity
@DiscriminatorValue("1")
public class DHTSensor extends Device {
    private final static Logger LOGGER = Logger.getLogger(DeviceDao.class.getName());
    @Column(name = "dscp")
    private String loc;

    @Override
    public NetData toNetData() {
        return NetData.from(getId()).setLongVal(getLocation()).
                setShortVal(Util.toIntString(getLastTemperature()),Util.toIntString(getLastHumidity()));
    }

    @Override
    public void updateFromNetData(NetData netData) {
        DeviceData deviceData = new DeviceData();
        deviceData.setId(this.getDeviceDataList().size() + 1);
        deviceData.setDevice(this);
        deviceData.setNumberValue1((double)netData.getShortIntVal2());
        deviceData.setNumberValue2((double)netData.getShortIntVal1());
        deviceData.setInsertDate(LocalDateTime.now());
        if (this.getDeviceDataList().isEmpty()) {
            this.getDeviceDataList().add(deviceData);
        }
        else {
           if(!deviceData.equals(getDeviceDataList().get(0))) {
               this.getDeviceDataList().add(deviceData);
           }
        }

    }


    public double getLastTemperature() {
        if (this.getDeviceDataList().isEmpty()) return 0;
        return  this.getDeviceDataList().get(0).getNumberValue2();
    }

    public double getLastHumidity() {
        if (this.getDeviceDataList().isEmpty()) return 0;
        return  this.getDeviceDataList().get(0).getNumberValue1();
    }

    public String getLocation() {
        return loc;
    }

    public void setLocation(String loc) {
        this.loc = loc;
    }

}
