package com.adipro.devices;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name="device_generic_data")
@IdClass(DeviceDataId.class)
public class DeviceData {
    @Id
    @Column(name = "id")
    private Integer id;

    @Id
    @ManyToOne
    @JoinColumn(name="device_id")
    private Device device;

    @Column(name="insert_tmttmp")
    private LocalDateTime insertDate;
    @Column(name="num1")
    private Double numberValue1;
    @Column(name="num2")
    private Double numberValue2;
    @Column(name="num3")
    private Double numberValue3;
    @Column(name="str1")
    private String stringValue1;
    @Column(name="str2")
    private String stringValue2;
    @Column(name="str3")
    private String stringValue3;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public Double getNumberValue1() {
        return numberValue1;
    }

    public void setNumberValue1(Double numberValue1) {
        this.numberValue1 = numberValue1;
    }

    public Double getNumberValue2() {
        return numberValue2;
    }

    public void setNumberValue2(Double numberValue2) {
        this.numberValue2 = numberValue2;
    }

    public Double getNumberValue3() {
        return numberValue3;
    }

    public void setNumberValue3(Double numberValue3) {
        this.numberValue3 = numberValue3;
    }

    public String getStringValue1() {
        return stringValue1;
    }

    public void setStringValue1(String stringValue1) {
        this.stringValue1 = stringValue1;
    }

    public String getStringValue2() {
        return stringValue2;
    }

    public void setStringValue2(String stringValue2) {
        this.stringValue2 = stringValue2;
    }

    public String getStringValue3() {
        return stringValue3;
    }

    public void setStringValue3(String stringValue3) {
        this.stringValue3 = stringValue3;
    }

    private boolean compareObjects(Object obj1,Object obj2) {
        if(obj1 == null && obj2 == null) return true;
        if (obj1 != null) return obj1.equals(obj2);
        if (obj2 != null) return obj2.equals(obj1);
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.insertDate.format(DateTimeFormatter.ISO_DATE_TIME) + " : ");
        if (this.getNumberValue1() != null) sb.append(this.getNumberValue1() + " ");
        if (this.getNumberValue2() != null) sb.append(this.getNumberValue2() + " ");
        if (this.getNumberValue3() != null) sb.append(this.getNumberValue3() + " ");
        if (this.getStringValue1() != null) sb.append(this.getStringValue1() + " ");
        if (this.getStringValue2() != null) sb.append(this.getStringValue2() + " ");
        if (this.getStringValue3() != null) sb.append(this.getStringValue3() + " ");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (! (obj instanceof  DeviceData)) return false;
        if (obj == null) return false;
        DeviceData other = (DeviceData) obj;

        if (!compareObjects(this.getNumberValue1(),other.getNumberValue1())) return false;
        if (!compareObjects(this.getNumberValue2(),other.getNumberValue2())) return false;
        if (!compareObjects(this.getNumberValue3(),other.getNumberValue3())) return false;
        if (!compareObjects(this.getStringValue1(),other.getStringValue1())) return false;
        if (!compareObjects(this.getStringValue2(),other.getStringValue2())) return false;
        if (!compareObjects(this.getStringValue3(),other.getStringValue3())) return false;
        return true;
    }
}
