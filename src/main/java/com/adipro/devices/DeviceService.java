package com.adipro.devices;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class DeviceService {

    @PersistenceContext(unitName="EclipseLink")
    EntityManager entitymanager;
    DeviceDao deviceDao;

    public Device getDevice(String id) {
       return deviceDao.findDevice(id);
    }

    public List<Device> getDevices() {
        return deviceDao.getAllDevices();
    }

    public List<DHTSensor> getDHTSensors() {
        return deviceDao.getAllDevices().stream().filter(device -> DHTSensor.class.isAssignableFrom(device.getClass())).
                map(d -> (DHTSensor)d).collect(Collectors.toList());
    }
    @Asynchronous
    public void asyncUpdateDevice(String id,NetData netData) {
        Device device = getDevice(id);
        device.update(netData);
        deviceDao.updateDevice(device);
    }
    @Asynchronous
    public void asyncUpdateDeviceStart(String id) {
        Device device = getDevice(id);
        device.setStarted(LocalDateTime.now());
        deviceDao.updateDevice(device);
    }

    public void asyncAddDevice(String id,String dscp,String type) {
        deviceDao.addDevice(id,dscp,deviceDao.findDeviceType(Integer.parseInt(type)));
    }

    @Asynchronous
    public void asyncUpdateDeviceMem(String id,int freeMem) {
        Device device = getDevice(id);
        device.setFreeMem(freeMem);
        device.setLastOnline(LocalDateTime.now());
        deviceDao.updateDevice(device);
    }


    @PostConstruct
    private void init(){
        deviceDao = new DeviceDao(entitymanager);
    }

}
