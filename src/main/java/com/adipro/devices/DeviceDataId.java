package com.adipro.devices;

public class DeviceDataId {
    private Integer id;
    private String device;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((device == null) ? 0 : device.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof DeviceDataId)) {
            return false;
        }
        if (obj == null) return false;
        if (obj == this) return true;
        else {
            if (((DeviceDataId) obj).id.equals(this.id) &&
                    ((DeviceDataId) obj).device.equals(this.device)) return true;
            return false;
        }
    }
}
