package com.adipro.ssh;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.TransportException;

import java.io.IOException;

public abstract class SSHAbstractCommandSender implements ISSHCommandSender {

    protected SSHClient client;
    @Override
    public boolean sendCommand(CommandProducer pr) {
        SSHClient client = getClient();
        if (client != null) {
            try {
                Session session = null;
                try {
                    session = client.startSession();
                    final Session.Command cmd = session.exec(pr.getCommandString());
                } catch (ConnectionException e) {
                    e.printStackTrace();
                    return false;
                } catch (TransportException e) {

                    e.printStackTrace();
                    return false;
                } finally {
                    // called even after return
                    if (session != null) {
                        session.close();
                    }

                    client.disconnect();
                }
            } catch (IOException io) {
                // nothing
            }
            return true;
        }

        return false;
    }

    protected abstract SSHClient getClient() ;

}
