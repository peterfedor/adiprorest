package com.adipro.ssh;

import net.schmizz.sshj.connection.channel.direct.Session;

public interface CommandProducer {
    String getCommandString();
}
