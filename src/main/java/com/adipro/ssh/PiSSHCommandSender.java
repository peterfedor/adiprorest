package com.adipro.ssh;

import com.adipro.annotation.SSHPi;
import net.schmizz.sshj.SSHClient;

import javax.enterprise.context.RequestScoped;

@RequestScoped
@SSHPi
public class PiSSHCommandSender extends SSHAbstractCommandSender{


    @Override
    protected SSHClient getClient() {
        return null;
    }
}
