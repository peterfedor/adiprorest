package com.adipro.ssh;

public interface ISSHCommandSender {
    boolean sendCommand(CommandProducer producer);
}
