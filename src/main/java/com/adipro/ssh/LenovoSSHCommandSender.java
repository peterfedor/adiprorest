package com.adipro.ssh;

import com.adipro.annotation.SSHLenovo;
import com.adipro.server.Properties;
import com.adipro.server.Server;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import javax.enterprise.context.RequestScoped;
import java.io.IOException;
import java.security.PublicKey;

@RequestScoped
@SSHLenovo
public class LenovoSSHCommandSender extends SSHAbstractCommandSender {
    final String LENOVOADDRESS = "LENOVOADDRESS";
    final String LENOVOUSER = "LENOVOUSER";
    final String LENOVOPORT = "LENOVOPORT";
    final String PWD = "PWD";
    @Override
    protected SSHClient getClient() {
        SSHClient sshClient = new SSHClient();
        try {
            sshClient.addHostKeyVerifier( (var1,var2,var3) -> {return true;} );

            sshClient.connect(Properties.getValue(LENOVOADDRESS),
                    Properties.getIntValue(LENOVOPORT));
            sshClient.authPassword(Properties.getValue(LENOVOUSER),
                    Properties.getValue(PWD));
            return sshClient;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

}
