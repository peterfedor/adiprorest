package com.adipro.service;


import com.adipro.devices.DHTSensor;
import com.adipro.schedule.TimeEvent;
import com.adipro.server.Server;
import com.adipro.server.ScheduleTasksManager;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Path("/admin")
@RequestScoped
public class AdminRest {
    @EJB
    Server server;

    @Path("add/{ip}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String addIP(@PathParam("ip")String ip) {
        server.addIp(ip);
        return Boolean.toString(true);

    }

    @Path("reset/")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String reset() {
        server.initIps();
        return Boolean.toString(true);

    }
    @Path("/health")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getCounter() {
        return server.getLastHealthCheck().format(DateTimeFormatter.ISO_DATE_TIME);
    }

}
