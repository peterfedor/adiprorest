package com.adipro.service;
import com.adipro.devices.DHTSensor;
import com.adipro.devices.DeviceService;
import com.adipro.devices.NetData;
import com.adipro.devices.Device;
import org.bouncycastle.util.Strings;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Path("/device")
@RequestScoped
public class DeviceRest {

    @Inject
    private HttpServletRequest request;
    @EJB
    DeviceService deviceService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getDevices() {
        StringBuilder sb = new StringBuilder();
        List<Device> devices = deviceService.getDevices();
        devices.forEach(device -> sb.append(device.toNetData().toStringId()));
        return devices.size() + sb.toString();
    }

    @Path("/{deviceId}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String receiveCommand(@PathParam("deviceId")String deviceId,
                                 @QueryParam("net") String netData) {
        deviceService.asyncUpdateDevice(deviceId, NetData.fromRawData(netData));
        return Boolean.toString(true);
        
    }

    @Path("/stats/{deviceId}")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getStats(@PathParam("deviceId")String deviceId) {
        Device device = deviceService.getDevice(deviceId);
        return "Started: " + device.getStarted().format(DateTimeFormatter.ISO_DATE_TIME) + "\n" +
                "Last Online: " + device.getLastOnline().format(DateTimeFormatter.ISO_DATE_TIME) + "\n" +
                "Free memory: " + String.valueOf(device.getFreeMem());
    }

    @Path("/stats/all")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getStatsAll() {
        StringBuilder sb = new StringBuilder();
        List<DHTSensor> devices = deviceService.getDHTSensors();
        devices.forEach(device -> sb.append(
                String.format("%1$-11s", device.getLocation()) +
                "Started: " + device.getStarted().format(DateTimeFormatter.ISO_DATE_TIME) + " " +
                        "Last Online: " + device.getLastOnline().format(DateTimeFormatter.ISO_DATE_TIME) + "\n"
        ));
        return sb.toString();
    }

    @Path("/started/{deviceId}")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getStarted(@PathParam("deviceId")String deviceId) {
        return deviceService.getDevice(deviceId).getStarted().format(DateTimeFormatter.ISO_DATE_TIME).substring(0,16);
    }

    @Path("/start/{deviceId}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String receiveStarted(@PathParam("deviceId")String deviceId) {
        deviceService.asyncUpdateDeviceStart(deviceId);
        return Boolean.toString(true);
    }

    @Path("/freemem/{deviceId}")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getFreeMem(@PathParam("deviceId")String deviceId) {
        return String.valueOf(deviceService.getDevice(deviceId).getFreeMem());
    }

    @Path("/freemem/{deviceId}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String setFreeMem(@PathParam("deviceId")String deviceId,
                                 @QueryParam("mem") String freeMem) {
        deviceService.asyncUpdateDeviceMem(deviceId,Integer.parseInt(freeMem));
        return Boolean.toString(true);
    }

    @Path("/data/{deviceId}")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getData(@PathParam("deviceId")String deviceId) {
        StringBuilder sb = new StringBuilder();
        deviceService.getDevice(deviceId).getDeviceDataList().forEach(nd -> sb.append(nd.toString() + "\n"));
        return sb.toString();    }


    @Path("/add/{deviceId}")
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String addDevice(@PathParam("deviceId")String deviceId,
                            @QueryParam("dscp") String dscp,
                            @QueryParam("typ") String type) {
        deviceService.asyncAddDevice(deviceId, dscp,type);
        return Boolean.toString(true);

    }


    // get data from device in case temp device temperature plus humidity
    @Path("/{deviceId}")
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getLastVal(@PathParam("deviceId") String id) {
        Device device = deviceService.getDevice(id);
        return ( device != null)? device.toNetData().toString():null;
    }

}
