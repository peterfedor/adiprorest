package com.adipro.service;

import com.adipro.annotation.SSHLenovo;
import com.adipro.ssh.ISSHCommandSender;
import com.adipro.webstream.StopCommand;
import com.adipro.webstream.WebStreamLoader;
import com.adipro.webstream.Webstream;
import com.adipro.webstream.Webstreams;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("/lenovoservice")
@RequestScoped
public class LenovoRest {
        @Inject @SSHLenovo
        ISSHCommandSender sshCommand;

        @EJB
        WebStreamLoader loader;

        @GET
        @Produces(MediaType.APPLICATION_JSON)
        public Webstreams getWebstreams() {
                Webstreams web =  new Webstreams();
                web.setWebstreamList(loader.getWebstreams());
                return web;
        }

        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.TEXT_PLAIN)
        public String sendCommand(Webstream webstream) {
                System.out.println("receiving REST POST for : " + webstream);
                return Boolean.toString(sshCommand.sendCommand(webstream));
        }

        @Path("/stop")
        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.TEXT_PLAIN)
        public String sendStopCommand(Webstream webstream) {
                return Boolean.toString(sshCommand.sendCommand(new StopCommand()));
        }


}
