package com.adipro.service;

import com.adipro.annotation.SSHPi;
import com.adipro.server.Server;
import com.adipro.ssh.ISSHCommandSender;
import com.adipro.webstream.WebStreamLoader;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/piservice")
@RequestScoped
public class PiRest {
        @Inject @SSHPi
        ISSHCommandSender sshCommand;

        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String getTestService() {
            return "OK";
                    //Server.getInstance().getProperties().toString();
        }
}
