package com.adipro.tester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class RestTester implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(RestTester.class.getName());
    private String path;
    private Predicate<String> predicate;
    public RestTester(String url,Predicate<String> predicate) {
        // connect to
        this.path = url;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        HttpURLConnection con = null;
        try {
            URL url = new URL(path);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "text/plain");

            try(InputStream stream = con.getInputStream()) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                String line = bufferedReader.readLine();
                if( !predicate.test(line)) {
                    LOGGER.severe("url " + path + " returned " +  line);
                } else {
                    LOGGER.info(path + "OK");
                }
            }
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        finally {
            if (con != null ) con.disconnect();
        }
    }
}
