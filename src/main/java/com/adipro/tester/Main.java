package com.adipro.tester;

public class Main {

    private static final int SLEEPTIME=15*60*1000;
    public static void main(String[] args) throws Exception{
        System.setProperty("java.net.useSystemProxies", "true");
        RestTester a001 = new RestTester("http://opc-vm1.adipro.eu:8080/adiprorest/rest/device/A001","A001  Obyvak  17  55 "::equals);
        RestTester a002 = new RestTester("http://opc-vm1.adipro.eu:8080/adiprorest/rest/device/A002","A002  BALKON  18  55 "::equals);
        RestTester a003 = new RestTester("http://opc-vm1.adipro.eu:8080/adiprorest/rest/device/A003","A003  SPALNA  30  55 "::equals);

        while(true) {
            a001.run();
            a002.run();
            a003.run();
            Thread.sleep(SLEEPTIME);
        }
    }
}
