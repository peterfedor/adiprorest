package com.adipro.webstream;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "webstreams")
@XmlAccessorType(XmlAccessType.FIELD)
public class Webstreams {

    @XmlElement(name="webstreamList")
    private List<Webstream> webstreamList;

    public List<Webstream> getWebstreamList() {
        return webstreamList;
    }

    public void setWebstreamList(List<Webstream> webstreamList) {
        this.webstreamList = webstreamList;
    }

}
