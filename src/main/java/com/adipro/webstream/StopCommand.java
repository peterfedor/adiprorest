package com.adipro.webstream;

import com.adipro.ssh.CommandProducer;

public class StopCommand implements CommandProducer {
    @Override
    public String getCommandString() {
        return "killall vlc";
    }
}
