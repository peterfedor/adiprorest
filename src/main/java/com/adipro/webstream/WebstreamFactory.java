package com.adipro.webstream;

import com.adipro.common.LoadableFactory;

public class WebstreamFactory extends LoadableFactory<Webstream> {
    @Override
    public Webstream createInstance(String from) {
        return new Webstream();
    }
}
