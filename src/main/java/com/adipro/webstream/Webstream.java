package com.adipro.webstream;

import com.adipro.common.Loadable;
import com.adipro.ssh.CommandProducer;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.StringTokenizer;
@XmlRootElement
public class Webstream implements Loadable, CommandProducer {
    private String name;
    private String address;

    @Override
    public void load(String line) {
        StringTokenizer token = new StringTokenizer(line,";");
        name = token.nextToken();
        address = token.nextToken();
    }

    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    @JsonIgnore
    public String getCommandString() {
        return "cvlc " + address + " &";
    }

    @Override
    public String toString() {
        return name + " " + address;
    }
}
