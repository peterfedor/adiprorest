package com.adipro.webstream;

import com.adipro.server.ObjectLoader;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.List;

@Singleton
@Startup
public class WebStreamLoader {

    private final String RADIOSFILE = "radios.list";
    ObjectLoader<Webstream> loader;
    List<Webstream> webstreams;
    @PostConstruct
    private void init() {
        loader = new ObjectLoader<>();
        try {
            webstreams = loader.loadObjects(RADIOSFILE,new WebstreamFactory());
        } catch (Exception e) {

        }
    }

    @PreDestroy
    private void destroy() {
        this.webstreams.clear();
    }

    public List<Webstream> getWebstreams() {return webstreams;}

}
