package com.adipro.filters;
import com.adipro.server.Server;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@PreMatching
public class AdiproServiceFilter  implements ContainerRequestFilter {
    @EJB
    Server server;

    @Context
    private HttpServletRequest httpServletRequest;
    public void filter(ContainerRequestContext ctx) throws IOException {

        if (!server.isIPAddressAllowed(httpServletRequest.getRemoteAddr())) {
            throw new NotAuthorizedException("Not Authorized.");
        }
    }

}