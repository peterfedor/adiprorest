package com.adipro.server;

import com.adipro.common.Loadable;
import com.adipro.common.LoadableFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

public class ObjectLoader<T extends Loadable> {

    public List<T> loadObjects(String objectFile, LoadableFactory<T> factory) throws Exception{
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream(objectFile);
        return new BufferedReader(new InputStreamReader(input)).lines().map(str ->{ T object = factory.createInstance(str);object.load(str);return object; }).collect(Collectors.toList());
    }
}
