package com.adipro.server;

import com.adipro.schedule.TimeEvent;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Singleton
@Startup
public class ScheduleTasksManager {
    @Inject
    Event<TimeEvent> timeEvent;


    @Schedule(hour = "*", minute = "*/30", info = "Every 1/2 hour timer",persistent = false)
    public void fireOnceADayEvent() {
        System.out.println("Firing once a day event.");
        timeEvent.fire(new TimeEvent());
    }

}
