package com.adipro.server;

public class Properties {
    public static String getValue(String name) {
        return Server.getInstance().getProperties().getProperty(name);
    }

    public static int getIntValue(String name) {
        return Integer.valueOf(getValue(name));
    }
}
