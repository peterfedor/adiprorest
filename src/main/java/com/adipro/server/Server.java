package com.adipro.server;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.IOException;
import java.io.InputStream;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;


@Singleton
@Startup
public class Server {

    private static Server instance;
    //instance
    private Properties properties;
    private HashSet<String> allowedIPs = new HashSet<>();
    private LocalDateTime lastHealthCheck = LocalDateTime.now();

    @PostConstruct
    private void init() {
        instance = this;
        loadProperties();
        initIps();
    }

    @PreDestroy
    private void destroy() {
        this.properties.clear();
    }

    public static Server getInstance() {
        return instance;
    }

    private void loadProperties() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("server.properties");
        properties = new Properties();
        try {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public boolean isIPAddressAllowed(String ip) {
        return isLocalhost(ip) || allowedIPs.contains(ip);
    }

    private boolean isLocalhost(String ip) {
        return "193.122.5.165".equals(ip);
    }
    public void addIp(String ip) {
       allowedIPs.add(ip);
    }

    public void initIps() {
        allowedIPs.clear();
        Arrays.stream(String.valueOf(properties.get("ALLOWEDIPS")).split(";")).forEach(allowedIPs::add);
    }

    public LocalDateTime getLastHealthCheck() {
        return lastHealthCheck;
    }

    public void setLastHealthCheck(LocalDateTime lastHealthCheck) {
        this.lastHealthCheck = lastHealthCheck;
    }

    public String getGmailPsw() {
        if(properties.getProperty("GMAILPWD") != null ) {
            return properties.getProperty("GMAILPWD");
        }
        return "";
    }
}
