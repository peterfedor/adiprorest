package com.adipro.common;

@FunctionalInterface
public interface Loadable {
    void load(String object);
}
