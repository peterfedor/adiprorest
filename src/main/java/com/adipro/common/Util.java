package com.adipro.common;

import java.text.DecimalFormat;

public class Util {
    public static String toIntString(double d) {
        return new DecimalFormat("#").format(d);
    }
}
