package com.adipro.common;

public abstract class LoadableFactory<T extends Loadable> {
    public abstract T createInstance(String from);
}
